﻿using UnityEngine;
using System.Collections;
using TSP.TDEW.Game;
using System.Collections.Generic;
using System;

namespace TSP.TDEW.Core
{
    public sealed class WaveManager : DataManager<WaveManager>
    {
        private readonly List<WaveInfo> _waves = new List<WaveInfo>();
        private int _currentWaveIndex = 0;
        private bool _isRunned = false;

        private Timer _runWaveTimer = null;
        private Timer _waveDurationTimer = null;
        private Timer _spawnEnemyTimer = null;

        public WaveInfo CurrentWave { get { return _waves[_currentWaveIndex]; } }
        public int CurrentWaveIndex { get { return _currentWaveIndex; } }
        public int WaveCount { get { return _waves.Count; } }
        public bool IsCompleted { get { return _currentWaveIndex >= _waves.Count; } }

        public Action WaveChanged;
        public Action WavesCompleted;

        public WaveManager()
        {

        }

        public void Run()
        {
            GameManager gm = GameManager.Instance;
            _waves.AddRange(gm.GameData.GetWaves());
            _currentWaveIndex = -1;

            RunNextWave();
            _isRunned = true;
        }

        private void RunNextWave()
        {
            if (!ProfileManager.Instance.Data.IsCanPlay)
            {
                return;
            }

            _currentWaveIndex++;

            if (!IsCompleted)
            {
                _runWaveTimer = TimeManager.Instance.RunTimer(CurrentWave.DelayBeforeStart);
                _runWaveTimer.Complete += OnRunWaveTimerCompleted;

                WaveChanged?.Invoke();
            }
            else
            {
                WavesCompleted?.Invoke();
            }
        }

        private void OnRunWaveTimerCompleted()
        {
            _runWaveTimer.Complete -= OnRunWaveTimerCompleted;
            _runWaveTimer = null;

            _waveDurationTimer = TimeManager.Instance.RunTimer(CurrentWave.Duration);
            _waveDurationTimer.Complete += OnWaveDurationTimerCompleted;

            SpawnEnemy();
        }

        private void OnWaveDurationTimerCompleted()
        {
            _waveDurationTimer.Complete -= OnWaveDurationTimerCompleted;
            _waveDurationTimer = null;

            RunNextWave();
        }

        private void SpawnEnemy()
        {
            if (!ProfileManager.Instance.Data.IsCanPlay)
            {
                return;
            }

            var enemiesTypes = CurrentWave.GetEnemies();
            var currentEnemyTypeIndex = UnityEngine.Random.Range(0, enemiesTypes.Count);

            EnemyType enemyType = enemiesTypes[currentEnemyTypeIndex];

            GameManager.Instance.CreateEnemy(enemyType);

            _spawnEnemyTimer = TimeManager.Instance.RunTimer(CurrentWave.TimeSpawnEnemies);
            _spawnEnemyTimer.Complete += OnSpawnEnemyTimerCompleted;
        }

        private void OnSpawnEnemyTimerCompleted()
        {
            _spawnEnemyTimer.Complete -= OnSpawnEnemyTimerCompleted;
            _spawnEnemyTimer = null;

            if (!IsCompleted)
            {
                SpawnEnemy();
            }
        }

        public void Setup()
        {

        }

        public void Update()
        {
            TimeManager tm = TimeManager.Instance;
            ProfileManager pm = ProfileManager.Instance;

            if (!tm.IsPaused)
            {
            }
        }
    }
}

