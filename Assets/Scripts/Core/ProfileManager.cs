﻿using System;
using System.Collections;
using UnityEngine;
using TSP.TDEW.Game;

namespace TSP.TDEW.Core
{
    public sealed class ProfileData
    {
        public int HealthPoints { get; private set; }
		public int Coins { get; private set; }
        public bool IsCanPlay { get { return HealthPoints > 0; } }

        public ProfileData(PlayerInfo playerInfo)
        {
            HealthPoints = playerInfo.HealthPoints;
            Coins = playerInfo.Coins;
        }

        public void SubtractCoins(int value)
        {
            Coins -= value;
        }

        public void AppendCoins(int value)
        {
            Coins += value;
        }

        public void SubtractHealth(int value)
        {
            HealthPoints -= value;
        }
    }

    public sealed class ProfileManager : DataManager<ProfileManager>
    {
        public Action HealthPointsChanged;
        public Action CoinsChanged;
		public ProfileData Data { get; private set; }

		public ProfileManager()
        {

        }

        public bool TryPurchaseTower(TowerType towerType)
        {
            TowerBuildInfo towerBuildInfo = GameManager.Instance.GameData.GetTowerBuildInfo(towerType);

            if (Data.Coins >= towerBuildInfo.BuildPrice)
            {
                Data.SubtractCoins(towerBuildInfo.BuildPrice);
                CoinsChanged?.Invoke();
                return true;
            }

            return false;
        }

        public bool TrySellTower(TowerType towerType)
        {
            TowerBuildInfo towerBuildInfo = GameManager.Instance.GameData.GetTowerBuildInfo(towerType);
            Data.AppendCoins(towerBuildInfo.BuildPrice / 2);
            CoinsChanged?.Invoke();

            return true;
        }

        public bool TryReceiveRewardEnemy(EnemyType enemyType)
        {
            EnemyInfo enemyInfo = GameManager.Instance.GameData.GetEnemyInfo(enemyType);
            Data.AppendCoins(enemyInfo.Reward);
            CoinsChanged?.Invoke();

            return true;
        }

        public bool TryHitPlayer(int value)
        {
            if (Data.HealthPoints > 0)
            {
                Data.SubtractHealth(value);
                HealthPointsChanged?.Invoke();
                return true;
            }

            return false;
        }

        public void Setup(PlayerInfo playerInfo)
        {
            Data = new ProfileData(playerInfo);
        }

        public void Update()
        {

        }
    }
}

