﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TSP.TDEW.Core;
using System;

namespace TSP.TDEW.Game
{
    [System.Serializable]
    public sealed class TowerData
    {
        private Transform _transform;

        private ConstructionSlot _slot;
        private TowerBuildInfo _buildInfo;

        public bool IsBuilded { get { return _buildInfo != null; } }
        public TowerBuildInfo BuildInfo { get { return _buildInfo; } }
        public ConstructionSlot Slot { get { return _slot; } }

        public void SetTransform(Transform transform)
        {
            _transform = transform;
        }

        public void SetSlot(ConstructionSlot slot)
        {
            _slot = slot;
        }

        public void SetBuildInfo(TowerType towerType)
        {
            _buildInfo = GameManager.Instance.GameData.GetTowerBuildInfo(towerType);
        }

        public void Clear()
        {
            _buildInfo = null;

            ClearShell();
        }


        // вынести в отдельный класс

        private const float HIT_RANGE = 0.1f;

        [SerializeField]
        private GameObject _shell = null;

        [SerializeField]
        private SpriteRenderer _shellSpriteRenderer = null;

        private bool _isRunShell = false;
        private Timer _timerShootInterval = null;

        private Enemy _enemy = null;

        public GameObject Shell { get { return _shell; } }
        public Enemy Enemy { get { return _enemy; } }
        public bool IsRunShell { get { return _isRunShell; } }
        public bool IsCanShoot { get { return _timerShootInterval == null; } }

        public void RunShell(Enemy enemy)
        {
            _enemy = enemy;

            _shell.transform.position = _transform.position;
            _timerShootInterval = TimeManager.Instance.RunTimer(_buildInfo.ShootInterval);
            _timerShootInterval.Complete += OnShootIntervalCompleted;

            _shell.SetActive(true);
            _isRunShell = true;
        }

        public void MoveShell()
        {
            if (!_isRunShell)
            {
                return;
            }

            if (_enemy == null)
            {
                if (_isRunShell)
                {
                    ClearShell();
                }

                return;
            }

            Vector2 shellPosition = _shell.transform.position;
            Vector2 enemyPosition = _enemy.transform.position;

            float distance = Vector2.Distance(shellPosition, enemyPosition);

            if (distance > HIT_RANGE)
            {
                float speed = _buildInfo.ShellSpeed * TimeManager.Instance.CurrentDeltaTime;
                _shell.transform.position = Vector2.MoveTowards(shellPosition, enemyPosition, speed);

                Vector2 delta = enemyPosition - shellPosition;
                float angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;

                _shell.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
            else
            {
                _isRunShell = false;
                _shell.SetActive(false);

                _enemy.ReceiveHit(_buildInfo.Damage);
            }
        }

        private void OnShootIntervalCompleted()
        {
            _timerShootInterval.Complete -= OnShootIntervalCompleted;
            _timerShootInterval = null;
        }

        private void ClearShell()
        {
            _isRunShell = false;
            _shell.SetActive(false);

            if (_timerShootInterval != null)
            {
                _timerShootInterval.Complete -= OnShootIntervalCompleted;
                _timerShootInterval = null;
            }
        }

        /////////////////////////////////////////////////
    }
}

