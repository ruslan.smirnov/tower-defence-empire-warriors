﻿using UnityEngine;
using System.Collections;

namespace TSP.TDEW.Game
{
    [System.Serializable]
    public sealed class EnemyData
    {
        private const float POINT_RANGE = 0.05f;

        [SerializeField]
        private Transform _transform = null;

        [SerializeField]
        private SpriteRenderer _spriteRenderer = null;

        [SerializeField]
        private EnemyInfo _info = null;

        [SerializeField]
        private float _currentHealthAmount = default;

        [SerializeField]
        private int _currentPathPoint = 0;

        public float MovingSpeed { get { return _info.MovingSpeed; } }
        public float CurrentHealthAmount { get { return _currentHealthAmount; } }
        public bool IsHealthZero { get { return _currentHealthAmount <= 0; } }
        public bool IsMoveCompleted { get { return _currentPathPoint >= GameManager.Instance.GameData.GetCountMovePoints(); } }
        public bool IsInited { get { return _info != null; } }
        public int Damage { get { return _info.Damage; } }
        public EnemyType Type { get { return _info.Type; } }

        public void SetTransform(Transform transform)
        {
            _transform = transform;
        }

        public void SetInfo(EnemyType enemyType)
        {
            _info = GameManager.Instance.GameData.GetEnemyInfo(enemyType);

            _currentHealthAmount = _info.HealthAmount;
            _spriteRenderer.sprite = _info.Sprite;
            _transform.position = GetMovePosition();
        }

        public void SubtractHealth(float value)
        {
            _currentHealthAmount -= value;
        }

        public bool IsMoveToPointCompleted()
        {
            GameData gd = GameManager.Instance.GameData;

            Vector2 targetPosition = gd.GetMovePointEnemy(_currentPathPoint);
            Vector2 currentPosition = _transform.position;

            float distance = Vector2.Distance(currentPosition, targetPosition);

            return distance <= POINT_RANGE;
        }

        public void IncMovePoint()
        {
            _currentPathPoint++;
        }

        public Vector2 GetMovePosition()
        {
            GameData gd = GameManager.Instance.GameData;

            return gd.GetMovePointEnemy(_currentPathPoint);
        }
    }
}

