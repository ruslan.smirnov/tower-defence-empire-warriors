﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TSP.TDEW.Core;

namespace TSP.TDEW.Game
{
    public sealed class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        [SerializeField]
        private GameData _gameData = null;

        [SerializeField]
        private GameViewer _viewer = null;

        public GameData GameData { get { return _gameData; } }

        private bool _isPossibleVictory = false;

        public Enemy CreateEnemy(EnemyType enemyType)
        {
            Enemy enemy = Instantiate(_gameData.EnemyPrototype);
            enemy.Spawn(enemyType);

            return enemy;
        }

        public void RegisterTower(Tower tower)
        {
            _gameData.AddTower(tower);
        }

        public void UnregisterTower(Tower tower)
        {
            _gameData.RemoveTower(tower);
        }

        public void RegisterEnemy(Enemy enemy)
        {
            _gameData.AddEnemy(enemy);
        }

        public void UnregisterEnemy(Enemy enemy)
        {
            _gameData.RemoveEnemy(enemy);
        }

        private void OnChangedHealthPoints()
        {
            ProfileManager pm = ProfileManager.Instance;

            if (!pm.Data.IsCanPlay)
            {
                _viewer.ShowDefeatWindow();
            }

            _viewer.UpdateHealthPoints(pm.Data.HealthPoints);
        }

        private void OnChangedCoins()
        {
            ProfileManager pm = ProfileManager.Instance;

            _viewer.UpdateCoins(pm.Data.Coins);
        }

        private void OnChangedWave()
        {
            WaveManager wm = WaveManager.Instance;
            int current = wm.CurrentWaveIndex;
            int max = wm.WaveCount;

            _viewer.UpdateWavesCounter(current, max);
        }

        private void OnCompletedWaves()
        {
            ProfileManager pm = ProfileManager.Instance;

            if (!pm.Data.IsCanPlay)
            {
                return;
            }

            _isPossibleVictory = true;
        }

        public void StartGame()
        {
            WaveManager.Instance.Run();
        }

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            ProfileManager pm = ProfileManager.Instance;
            WaveManager wm = WaveManager.Instance;

            pm.HealthPointsChanged += OnChangedHealthPoints;
            pm.CoinsChanged += OnChangedCoins;
            wm.WaveChanged += OnChangedWave;
            wm.WavesCompleted += OnCompletedWaves;

            _viewer.UpdateHealthPoints(pm.Data.HealthPoints);
            _viewer.UpdateCoins(pm.Data.Coins);
            _viewer.UpdateWavesCounter(wm.CurrentWaveIndex, wm.WaveCount);
        }

        private void Update()
        {
            TimeManager tm = TimeManager.Instance;

            if (Input.GetKeyDown(KeyCode.U))
            {
                tm.UnPause();
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                tm.Pause();
            }

            if (!tm.IsPaused)
            {
                List<Tower> towers = _gameData.GetTowers();
                List<Enemy> enemies = _gameData.GetEnemies();

                foreach (Tower tower in towers)
                {
                    if (tower.Data.IsCanShoot)
                    {
                        foreach(Enemy enemy in enemies)
                        {
                            tower.TryShoot(enemy);
                        }
                    }
                }

                if (_isPossibleVictory && enemies.Count == 0 && ProfileManager.Instance.Data.IsCanPlay)
                {
                    _isPossibleVictory = false;
                    _viewer.ShowVictoryWindow();
                }

                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    tm.DecTime();
                }

                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    tm.IncTime();
                }
            }
        }

        private void OnDestroy()
        {
            Instance = null;

            ProfileManager pm = ProfileManager.Instance;
            WaveManager wm = WaveManager.Instance;

            pm.HealthPointsChanged -= OnChangedHealthPoints;
            pm.CoinsChanged -= OnChangedCoins;
            wm.WaveChanged -= OnChangedWave;
            wm.WavesCompleted -= OnCompletedWaves;
        }
    }
}

