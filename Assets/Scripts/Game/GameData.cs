﻿using UnityEngine;
using System.Collections.Generic;

namespace TSP.TDEW.Game
{
    [System.Serializable]
    public sealed class GameData
    {
        [SerializeField]
        private List<TowerBuildInfo> _towerBuilds;

        private readonly List<Tower> _towers = new List<Tower>();

        [SerializeField]
        private List<EnemyInfo> _enemiesInfo = null;

        private readonly List<Enemy> _enemies = new List<Enemy>();

        [SerializeField]
        private List<GameObject> _pathEnemies = new List<GameObject>();

        [SerializeField]
        private List<WaveInfo> _waves = new List<WaveInfo>();

        [SerializeField]
        private Enemy _enemyPrototype = null;

        public TowerBuildInfo GetTowerBuildInfo(TowerType towerType)
        {
            return _towerBuilds.Find((x) => x.TowerType == towerType);
        }

        public List<Tower> GetTowers()
        {
            return new List<Tower>(_towers);
        }

        public void AddTower(Tower tower)
        {
            _towers.Add(tower);
        }

        public void RemoveTower(Tower tower)
        {
            _towers.Remove(tower);
        }

        public EnemyInfo GetEnemyInfo(EnemyType enemyType)
        {
            return _enemiesInfo.Find((x) => x.Type == enemyType);
        }

        public List<Enemy> GetEnemies()
        {
            return new List<Enemy>(_enemies);
        }

        public void AddEnemy(Enemy enemy)
        {
            _enemies.Add(enemy);
        }

        public void RemoveEnemy(Enemy enemy)
        {
            _enemies.Remove(enemy);
        }

        public List<GameObject> GetPathEnemies()
        {
            return new List<GameObject>(_pathEnemies);
        }

        public Vector2 GetMovePointEnemy(int index)
        {
            return _pathEnemies[index].transform.position;
        }

        public int GetCountMovePoints()
        {
            return _pathEnemies.Count;
        }

        public List<WaveInfo> GetWaves()
        {
            return new List<WaveInfo>(_waves);
        }

        public Enemy EnemyPrototype { get { return _enemyPrototype; } }
    }
}

