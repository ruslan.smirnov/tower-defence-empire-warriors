﻿using UnityEngine;
using System.Collections;
using System;
using TSP.TDEW.Core;

namespace TSP.TDEW.Game
{
    public sealed class Tower : MonoBehaviour
    {
        [SerializeField]
        private TowerData _data = null;

        [SerializeField]
        private TowerViewer _viewer = null;

        public TowerData Data { get { return _data; } }

        public Action TowerClicked;

        public void OnTowerButtonClick()
        {
            TowerClicked?.Invoke();
            _viewer.SetClickable(false);
        }

        public void Build(TowerType towerType)
        {
            _data.SetBuildInfo(towerType);

            _viewer.SetClickable(true);
            _viewer.Show();
        }

        public void Sell()
        {
            _data.Clear();
            _viewer.Hide();
        }

        private void OnSellMenuClosed()
        {
            _viewer.SetClickable(true);
        }

        public void TryShoot(Enemy enemy)
        {
            if (!_data.IsBuilded)
            {
                return;
            }

            if (!_data.IsCanShoot)
            {
                return;
            }

            Vector2 towerPosition = transform.position;
            float towerRange = _data.BuildInfo.Range;
            float distance = Vector2.Distance(transform.position, enemy.transform.position);

            if (distance <= towerRange)
            {
                _data.RunShell(enemy);
            }
        }

        public void Setup(ConstructionSlot slot)
        {
            _data.SetSlot(slot);
            _data.SetTransform(transform);
            _data.Slot.SellMenuClosed += OnSellMenuClosed;
        }

        private void Awake()
        {

        }

        private void OnDestroy()
        {
            _data.Slot.SellMenuClosed -= OnSellMenuClosed;
        }

        private void Update()
        {
            _data.MoveShell();
        }
    }
}

