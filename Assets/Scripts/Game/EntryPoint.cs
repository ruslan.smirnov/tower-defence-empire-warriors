﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

using TSP.TDEW.Core;

namespace TSP.TDEW.Game
{
    public sealed class EntryPoint : MonoBehaviour
    {
        [SerializeField]
        private GameObject _rootCanvas = null;

        [SerializeField]
        private PlayerInfo _playerInfo = null;

        private TimeManager _timeManager = null;
        private WaveManager _waveManager = null;
        private ProfileManager _profileManager = null;

        private void OnSceneGameLoaded(Scene scene, LoadSceneMode loadMode)
        {
            SceneManager.SetActiveScene(scene);
            SceneManager.sceneLoaded -= OnSceneGameLoaded;
        }

        private void Awake()
        {
            _timeManager = new TimeManager();
            _waveManager = new WaveManager();
            _profileManager = new ProfileManager();
        }

        private void Start()
        {
            _timeManager.Setup();
            _waveManager.Setup();
            _profileManager.Setup(_playerInfo);

            SceneManager.sceneLoaded += OnSceneGameLoaded;
            SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
        }

        private void Update()
        {
            _timeManager.Update();
            _waveManager.Update();
            _profileManager.Update();
        }
    }
}

