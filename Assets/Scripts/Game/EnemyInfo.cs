﻿using UnityEngine;
using System.Collections;

namespace TSP.TDEW.Game
{
    public enum EnemyType
    {
        None = 0,
        Enemy_0 = 1,
        Enemy_1 = 2,
        Enemy_2 = 3,
        Enemy_3 = 4,
    }

    [CreateAssetMenu(fileName = "EnemyInfo", menuName = "ScriptableObjects/EnemyInfo")]
    public class EnemyInfo : ScriptableObject
    {
        [SerializeField]
        private EnemyType _type = EnemyType.None;

        [SerializeField]
        private float _healthAmount = default;

        [SerializeField]
        private float _movingSpeed = default;

        [SerializeField]
        private int _damage = default;

        [SerializeField]
        private int _reward = default;

        [SerializeField]
        private Sprite _sprite = null;

        public EnemyType Type { get { return _type; } }
        public float HealthAmount { get { return _healthAmount; } }
        public float MovingSpeed { get { return _movingSpeed; } }
        public int Damage { get { return _damage; } }
        public int Reward { get { return _reward; } }
        public Sprite Sprite { get { return _sprite; } }
    }
}

