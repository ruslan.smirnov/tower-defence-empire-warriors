﻿using UnityEngine;
using System.Collections;
using TSP.TDEW.Core;

namespace TSP.TDEW.Game
{
    public sealed class Enemy : MonoBehaviour
    {
        [SerializeField]
        private EnemyData _data = null;

        [SerializeField]
        private EnemyViewer _viewer = null;

        public EnemyData Data { get { return _data; } }

        public void ReceiveHit(float value)
        {
            _data.SubtractHealth(value);
            _viewer.UpdateHealth();

            if (_data.IsHealthZero)
            {
                ProfileManager.Instance.TryReceiveRewardEnemy(_data.Type);

                GameManager.Instance.UnregisterEnemy(this);
                Destroy(gameObject);
            }
        }

        public void Spawn(EnemyType enemyType)
        {
            _data.SetInfo(enemyType);
            _viewer.UpdateViewer();

            GameManager.Instance.RegisterEnemy(this);
        }

        private void Move()
        {
            if (_data.IsMoveCompleted)
            {
                return;
            }

            if (_data.IsMoveToPointCompleted())
            {
                _data.IncMovePoint(); 
            }

            if (_data.IsMoveCompleted)
            {
                if (ProfileManager.Instance.TryHitPlayer(_data.Damage))
                {

                }

                GameManager.Instance.UnregisterEnemy(this);
                Destroy(gameObject);
                return;
            }

            float speed = _data.MovingSpeed * TimeManager.Instance.CurrentDeltaTime;
            Vector2 current = transform.position;
            Vector2 target = _data.GetMovePosition();

            transform.position = Vector2.MoveTowards(current, target, speed);
        }

        private void Awake()
        {
            
        }

        private void OnDestroy()
        {
            
        }

        private void Update()
        {
            if (!_data.IsInited)
            {
                return;
            }

            TimeManager tm = TimeManager.Instance;

            if (!tm.IsPaused)
            {
                Move();
            }
        }
    }
}

