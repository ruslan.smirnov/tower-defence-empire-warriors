﻿using UnityEngine;

namespace TSP.TDEW.Game
{
    [System.Serializable]
    public sealed class ConstructionSlotData
    {
        [SerializeField]
        private Tower _tower = null;

        public Tower Tower { get { return _tower; } }
    }
}

