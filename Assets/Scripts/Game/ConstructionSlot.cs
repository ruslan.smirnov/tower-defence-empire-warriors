﻿using System;
using System.Collections.Generic;
using TSP.TDEW.Core;
using UnityEngine;

namespace TSP.TDEW.Game
{
    public sealed class ConstructionSlot : MonoBehaviour
    {
        [SerializeField]
        private ConstructionSlotData _data = null;

        [SerializeField]
        private ConstructionSlotViewer _viewer = null;

        public Action SellMenuClosed; 

        public void OnBuildButtonClick(TowerType towerType)
        {
            if (_data.Tower.Data.IsBuilded)
            {
                return;
            }

            if (ProfileManager.Instance.TryPurchaseTower(towerType))
            {
                _data.Tower.Build(towerType);
                _viewer.Hide();

                GameManager.Instance.RegisterTower(_data.Tower);
            }
        }

        public void OnTowerButtonClicked()
        {
            _viewer.ShowSellMenu();
        }

        public void OnSellButtonClick()
        {
            if (!_data.Tower.Data.IsBuilded)
            {
                return;
            }

            if (ProfileManager.Instance.TrySellTower(_data.Tower.Data.BuildInfo.TowerType))
            {
                _data.Tower.Sell();
                _viewer.HideSellMenu();
                _viewer.Show();

                GameManager.Instance.UnregisterTower(_data.Tower);
            }
        }

        public void OnSellMenuClosed()
        {
            _viewer.HideSellMenu();

            SellMenuClosed?.Invoke();
        }

        private void Awake()
        {
            _data.Tower.Setup(this);
            _data.Tower.TowerClicked += OnTowerButtonClicked;
        }

        private void OnDestroy()
        {
            _data.Tower.TowerClicked -= OnTowerButtonClicked;
        }
    }
}

