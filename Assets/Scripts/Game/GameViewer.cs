﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace TSP.TDEW.Game
{
    public sealed class GameViewer : MonoBehaviour
    {
        [SerializeField]
        private GameManager _owner = null;

        [SerializeField]
        private TextMeshProUGUI _healthPointsText = null;

        [SerializeField]
        private TextMeshProUGUI _coinsText = null;

        [SerializeField]
        private TextMeshProUGUI _wavesCounterText = null;

        [SerializeField]
        private Button _startGameButton = null;

        [SerializeField]
        private GameObject _victoryWindow = null;

        [SerializeField]
        private GameObject _defeatWindow = null;

        public void UpdateHealthPoints(int value)
        {
            _healthPointsText.text = value.ToString();
        }

        public void UpdateCoins(int value)
        {
            _coinsText.text = value.ToString();
        }

        public void UpdateWavesCounter(int current, int max)
        {
            current++;
            _wavesCounterText.text = string.Format("{0}/{1}", current, max);
        }

        public void ShowVictoryWindow()
        {
            _victoryWindow.SetActive(true);
        }

        public void ShowDefeatWindow()
        {
            _defeatWindow.SetActive(true);
        }

        private void OnStartGameButtonClicked()
        {
            _owner.StartGame();
            _startGameButton.gameObject.SetActive(false);
        }

        private void Awake()
        {
            _startGameButton.onClick.AddListener(OnStartGameButtonClicked);
        }

        private void Start()
        {
            
        }

        private void OnDestroy()
        {
            _startGameButton.onClick.RemoveAllListeners();
        }
    }
}

