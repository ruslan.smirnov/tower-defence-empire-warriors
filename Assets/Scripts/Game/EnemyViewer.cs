﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace TSP.TDEW.Game
{
    public sealed class EnemyViewer : MonoBehaviour
    {
        [SerializeField]
        private Enemy _owner = null;

        [SerializeField]
        private Canvas _canvas = null;

        [SerializeField]
        private TextMeshProUGUI _currentHealthAmountText = null;

        public void UpdateViewer()
        {
            UpdateHealth();
        }

        public void UpdateHealth()
        {
            _currentHealthAmountText.text = _owner.Data.CurrentHealthAmount.ToString();
        }

        private void Awake()
        {
            _canvas.worldCamera = Camera.main;
        }

        private void OnDestroy()
        {
            
        }
    }
}


