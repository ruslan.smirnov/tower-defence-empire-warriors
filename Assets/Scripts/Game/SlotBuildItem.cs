﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TSP.TDEW.Game
{
    public sealed class SlotBuildItem : MonoBehaviour
    {
        private ConstructionSlotViewer _viewer;
        private ConstructionSlot _slot = null;

        [SerializeField]
        private GameObject _root = null;

        [SerializeField]
        private Button _buildButton = null;

        [SerializeField]
        private TowerType _buildType = TowerType.None;

        [SerializeField]
        private TextMeshProUGUI _buildPrice = null;

        public void Setup(ConstructionSlotViewer viewer, ConstructionSlot slot)
        {
            _viewer = viewer;
            _slot = slot;

            GameData gd = GameManager.Instance.GameData;
            TowerBuildInfo info = gd.GetTowerBuildInfo(_buildType);

            _buildPrice.text = gd.GetTowerBuildInfo(_buildType).BuildPrice.ToString();
            _buildButton.image.sprite = info.Sprite;
        }

        private void OnBuildTowerButtonClick()
        {
            _slot.OnBuildButtonClick(_buildType);
            _viewer.HideBuildMenu();
        }

        private void Awake()
        {
            _buildButton.onClick.AddListener(OnBuildTowerButtonClick);
        }

        private void OnDestroy()
        {
            _buildButton.onClick.RemoveAllListeners();
        }
    }
}

