﻿using UnityEngine;
using System.Collections;

namespace TSP.TDEW.Core
{
    [CreateAssetMenu(fileName = "PlayerInfo", menuName = "ScriptableObjects/PlayerInfo")]
    public sealed class PlayerInfo : ScriptableObject
    {
        [SerializeField]
        private int _healthPoints = default;

        [SerializeField]
        private int _coins = default;

        public int HealthPoints { get { return _healthPoints; } }
        public int Coins { get { return _coins; } }
    }
}

