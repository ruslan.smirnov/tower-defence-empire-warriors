﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace TSP.TDEW.Game
{
    public sealed class TowerViewer : MonoBehaviour
    {
        [SerializeField]
        private Tower _owner = null;

        [SerializeField]
        private GameObject _root = null;

        [SerializeField]
        private Button _button = null;

        public void Show()
        {
            _button.image.sprite = _owner.Data.BuildInfo.Sprite;

            _root.SetActive(true);
        }

        public void Hide()
        {
            _root.SetActive(false);
        }

        public void SetClickable(bool value)
        {
            _button.enabled = value;
        }

        private void Awake()
        {
            Hide();

            _button.onClick.AddListener(_owner.OnTowerButtonClick);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveAllListeners();
        }
    }
}

