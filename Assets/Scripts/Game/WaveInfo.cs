﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TSP.TDEW.Game
{
    [CreateAssetMenu(fileName = "WaveInfo", menuName = "ScriptableObjects/WaveInfo")]
    public sealed class WaveInfo : ScriptableObject
    {
        [SerializeField]
        private float _duration = default;

        [SerializeField]
        private List<EnemyType> _enemies = null;

        [SerializeField]
        private float _delayBeforeStart = default;

        [SerializeField]
        private float _timeSpawnEnemies = default;

        public float Duration { get { return _duration; } }
        public float DelayBeforeStart { get { return _delayBeforeStart; } }
        public float TimeSpawnEnemies { get { return _timeSpawnEnemies; } }

        public List<EnemyType> GetEnemies()
        {
            return new List<EnemyType>(_enemies);
        }
    }
}

