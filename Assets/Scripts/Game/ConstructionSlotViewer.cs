﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TSP.TDEW.Game
{
    public sealed class ConstructionSlotViewer : MonoBehaviour
    {
        [SerializeField]
        private ConstructionSlot _owner = null;

        [SerializeField]
        private GameObject _root = null;

        [SerializeField]
        private Button _slotButton = null;

        [SerializeField]
        private Button _clickBlocker = null;

        [SerializeField]
        private List<SlotBuildItem> _buildItems;

        // вынести в отдельный класс

        [SerializeField]
        private GameObject _rootSellMenu = null;

        [SerializeField]
        private Button _sellClickBlocker = null;

        [SerializeField]
        private Button _sellButton = null;

        public void ShowBuildMenu()
        {
            _root.SetActive(true);
            _clickBlocker.gameObject.SetActive(true);
        }

        public void HideBuildMenu()
        {
            _root.SetActive(false);
            _clickBlocker.gameObject.SetActive(false);
        }

        public void ShowSellMenu()
        {
            _rootSellMenu.SetActive(true);
            _sellClickBlocker.gameObject.SetActive(true);
        }

        public void HideSellMenu()
        {
            _rootSellMenu.SetActive(false);
            _sellClickBlocker.gameObject.SetActive(false);
        }

        public void Show()
        {
            _slotButton.gameObject.SetActive(true);
        }

        public void Hide()
        {
            _slotButton.gameObject.SetActive(false);
        }

        private void Awake()
        {
            _root.SetActive(false);

            _slotButton.onClick.AddListener(ShowBuildMenu);
            _clickBlocker.onClick.AddListener(HideBuildMenu);

            _rootSellMenu.SetActive(false);

            _sellClickBlocker.onClick.AddListener(_owner.OnSellMenuClosed);
            _sellButton.onClick.AddListener(_owner.OnSellButtonClick);

            _clickBlocker.gameObject.SetActive(false);
            _sellClickBlocker.gameObject.SetActive(false);
        }

        private void Start()
        {
            foreach (var item in _buildItems)
            {
                item.Setup(this, _owner);
            }
        }

        private void OnDestroy()
        {
            _slotButton.onClick.RemoveAllListeners();
            _clickBlocker.onClick.RemoveAllListeners();

            _sellClickBlocker.onClick.RemoveAllListeners();
            _sellButton.onClick.RemoveAllListeners();
        }
    }
}

