﻿using UnityEngine;
using System.Collections;

namespace TSP.TDEW.Game
{
    public enum TowerType
    {
        None = -1,
        Archer = 0,
        Barrak = 1,
        Magic = 2,
        Golem = 3,
    }

    [CreateAssetMenu(fileName = "TowerBuildInfo", menuName = "ScriptableObjects/Tower")]
    public sealed class TowerBuildInfo : ScriptableObject
    {
        [SerializeField]
        private TowerType _towerType = TowerType.None;

        [SerializeField]
        private int _buildPrice = default;

        [SerializeField]
        private float _range = default;

        [SerializeField]
        private float _shootInterval = default;

        [SerializeField]
        private float _damage = default;

        [SerializeField]
        private float _shellSpeed = default;

        [SerializeField]
        private Sprite _sprite = null;

        [SerializeField]
        private Sprite _iconBuildSprite = null;

        [SerializeField]
        private Sprite _shellSprite = null;

        public TowerType TowerType { get { return _towerType; } }
        public int BuildPrice { get { return _buildPrice; } }
        public float Range { get { return _range; } }
        public float ShootInterval { get { return _shootInterval; } }
        public float Damage { get { return _damage; } }
        public float ShellSpeed { get { return _shellSpeed; } }
        public Sprite Sprite { get { return _sprite; } }
        public Sprite IconBuildSprite { get { return _iconBuildSprite; } }
        public Sprite ShellSprite { get { return _shellSprite; } }
    }
}

