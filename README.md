# Tower Defence Empire warriors 

Unity Middle Developer Test Task

A tower defence taken from the game called Empire warriors td.

Slots and towers:
* a tower can be build on a special slot on the game map;
* upon clicking on the slot, a "Tower build" menu with all available tower types should appear;
* there are 4 types of tower;
* each tower has its own build price, range, shoot interval, and damage;
* any built tower can be sold upon pressing the "Sell" button from the "Tower build" menu;
* tower hits an enemy that is in its range and is the closest to the player castle.
* settings for each tower type should be contained in Scriptable Objects so it is easy for a game designer to tweak them;

Enemies:

* each enemy has its own health amount, moving speed, and damage;
* an enemy spawns outside the camera view and follows a certain path towards the player castle;
* when an enemy reaches the player castle, player loses amount of health points that is equal to the enemy damage;
* upon dying each enemy gives a player a random amount of coins (lower and upper random boundaries are unique to each type of enemy);
* when a tower hits an enemy, he loses certain amount of health points.
* settings for each enemy should be contained in Scriptable Objects so it is easy for a game designer to tweak them;

Enemy waves:
* enemies come in waves;
* each wave has its own duration;
* if the current wave duration has expired, the next wave stars;
* settings for each enemy wave should be contained in Scriptable Objects so it is easy for a game designer to tweak them;

UI:
* UI should be adaptable for different screens and resolutions;
* UI should be adaptable for different screens and resolutions;

* number of current wave;
* max amount of waves;
* current amount of player health points;
* current amount of player coins.

Please make one level with mechanics listed above implemented. Types of towers, amount of tower slots, waves, enemies etc are up to you to decide.
UI does not have to look the same as on the screenshots below, they are only provided so you could use something as a reference.
Feel free to use any free graphical assets but be aware that they will not have effect on task evaluation.
If you understand that you will not be able to make this task completely, do what you can. Being able to prioritize is a very important skill as well.

